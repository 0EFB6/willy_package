#!/bin/bash
set -euo pipefail
echo "$0"
full_path=$(realpath "$0")
dir_path=${full_path%/*}
readarray -t x86_list <<< "$(find x86_64/ -type f -name PKGBUILD | awk -F / '{print $2}')"

mkdir export

for x in "${x86_list[@]}"
do
    cd "${dir_path}"/x86_64/"${x}"
    echo "### Moving ${dir_path}/x86_64/${x} to willy-repo ###"
    mv *.tar.zst ../../export
    #find . -mindepth 1 -maxdepth 1 -type d -print0 | xargs -r0 rm -R
done
